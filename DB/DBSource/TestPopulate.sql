-- Populate Company Table --
INSERT into Company VALUES (ID, '1st Comp', '1111', '111@1111.222');
INSERT into Company VALUES (ID, '2nd Comp', '2222', '222@2222.222');
INSERT into Company VALUES (ID, '3rd Comp', '3333', '333@3333.333');

-- Populate Coupon Table --
INSERT into Coupon (TITLE, START_DATE, END_DATE, AMOUNT, TYPE, MESSAGE, PRICE, IMAGE) VALUES ('Food Coupon 1', '2001-01-30 14:00:00', '2001-02-15 13:00:15', 5, 'Food', 'Coupon for food 1. Use in any place that provides food 1', 0.99, 'https://cdn.cnn.com/cnnnext/dam/assets/171027052520-processed-foods-exlarge-tease.jpg');
INSERT into Coupon (TITLE, START_DATE, END_DATE, AMOUNT, TYPE, MESSAGE, PRICE, IMAGE) VALUES ('Food Coupon 2', '2002-01-30 15:10:20', '2002-02-15 16:13:13', 6, 'Food', 'Coupon for food 2. Use in any place that provides food 2', 2.99, 'https://www.justataste.com/wp-content/uploads/2013/05/easy-homemade-parmesan-hamburger-buns-recipe.jpg');
INSERT into Coupon (TITLE, START_DATE, END_DATE, AMOUNT, TYPE, MESSAGE, PRICE, IMAGE) VALUES ('Vacation Coupon 1', '2010-05-25 16:34:52', '2011-10-21 11:47:39', 500, 'Vacation', 'Coupon for vacation 1. Only usable with 3rd Company', 50.0, 'https://s-ec.bstatic.com/images/hotel/max1280x900/101/101430248.jpg');
INSERT into Coupon (TITLE, START_DATE, END_DATE, AMOUNT, TYPE, MESSAGE, PRICE, IMAGE) VALUES ('Vacation Coupon 2', '2011-05-25 17:42:34', '2015-12-21 15:57:26', 500, 'Vacation', 'Coupon for vacation 2. Only usable with 2nd Company', 40.0, 'https://s-ec.bstatic.com/images/hotel/max1280x900/101/101430248.jpg');

-- Populate Customer Table --
INSERT into Customer (CUST_NAME, PASSWORD) VALUES ('1st Customer', '1111');
INSERT into Customer (CUST_NAME, PASSWORD) VALUES ('2nd Customer', '2222');
INSERT into Customer (CUST_NAME, PASSWORD) VALUES ('3rd Customer', '3333');
INSERT into Customer (CUST_NAME, PASSWORD) VALUES ('4th Customer', '4444');

-- Populate Company-Coupon Table --
INSERT into Company_Coupon (COMP_ID, COUPON_ID) VALUES (1, 1);
INSERT into Company_Coupon (COMP_ID, COUPON_ID) VALUES (1, 2);
INSERT into Company_Coupon (COMP_ID, COUPON_ID) VALUES (3, 3);
INSERT into Company_Coupon (COMP_ID, COUPON_ID) VALUES (2, 4);

-- Populate Customer-Coupon Table --
INSERT into Customer_Coupon (CUST_ID, COUPON_ID) VALUES (1, 1);
INSERT into Customer_Coupon (CUST_ID, COUPON_ID) VALUES (1, 2);
INSERT into Customer_Coupon (CUST_ID, COUPON_ID) VALUES (1, 4);
INSERT into Customer_Coupon (CUST_ID, COUPON_ID) VALUES (2, 2);
INSERT into Customer_Coupon (CUST_ID, COUPON_ID) VALUES (2, 4);
INSERT into Customer_Coupon (CUST_ID, COUPON_ID) VALUES (3, 1);
INSERT into Customer_Coupon (CUST_ID, COUPON_ID) VALUES (3, 2);