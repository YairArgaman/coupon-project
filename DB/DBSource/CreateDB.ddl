-- For creating new database schema only!!! --
DROP SCHEMA IF EXISTS Coupon_System;
CREATE SCHEMA IF NOT EXISTS Coupon_System;
-- ! Execute this before executing the next queries ! --
USE Coupon_System;

-- Execute this AFTER Executing the USE command --
-- For fresh table creation only --
DROP TABLE IF EXISTS Company_Coupon;
DROP TABLE IF EXISTS Customer_Coupon;
DROP TABLE IF EXISTS Company;
DROP TABLE IF EXISTS Coupon;
DROP TABLE IF EXISTS Customer;

  CREATE TABLE IF NOT EXISTS Company (
    ID bigint NOT NULL AUTO_INCREMENT,
    COMP_NAME varchar(255),
    PASSWORD varchar(255),
    EMAIL varchar(255),
    PRIMARY KEY (ID)
  );
--   SHOW CREATE TABLE Company;

  CREATE TABLE IF NOT EXISTS Customer (
    ID bigint NOT NULL AUTO_INCREMENT,
    CUST_NAME varchar(255),
    PASSWORD varchar(255),
    PRIMARY KEY (ID)
  );

  CREATE TABLE IF NOT EXISTS Coupon (
    ID bigint NOT NULL AUTO_INCREMENT,
    TITLE varchar(255), -- Short coupon descriptor/title
    START_DATE timestamp,
    END_DATE timestamp,
    AMOUNT int,
    TYPE enum (RESTAURANTS,
      'Electronics',
      'Food',
      'Health',
      'Sports',
      'Camping',
      'Traveling',
      'Hobbies',
      'Entertainment' ), -- Add more as needed in host program using the following SQL statement:
                                  -- ALTER TABLE Coupon MODIFY COLUMN TYPE enum (/* all values here */)
    MESSAGE varchar(2048),
    PRICE double,
    IMAGE varchar(2048), -- Link/Path of relevant picture (provided by company)
    PRIMARY KEY (ID)
  );

  CREATE TABLE IF NOT EXISTS Company_Coupon (
    COMP_ID bigint NOT NULL,
    COUPON_ID bigint NOT NULL,
    FOREIGN KEY (COMP_ID) REFERENCES Company(ID),
    FOREIGN KEY (COUPON_ID) REFERENCES Coupon(ID)
  );

  CREATE TABLE IF NOT EXISTS Customer_Coupon (
    CUST_ID bigint NOT NULL,
    COUPON_ID bigint NOT NULL,
    FOREIGN KEY (CUST_ID) REFERENCES Customer(ID),
    FOREIGN KEY (COUPON_ID) REFERENCES Coupon(ID)
  );