coupon-project
===========

John Bryce course's Coupon System project

####To begin use:
#####Create DB:
    1. Connect to a MySQL DB
    2. Execute CreateDB.ddl. If that doesn't work, open the file and execute in the following order:
        1. Execute the first `DROP` and `CREATE` lines.
        2. Execute the `USE Coupon_System` line.
        3. Execute the remaining commands.
    3. Execute `TestPopulate.sql`.
    4. Check DB by running `CheckDB.sql`.
    
    
Things to check if to change:
===========
    1. Replace ConnectionPool with HikariCP?
    2. 