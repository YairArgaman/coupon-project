package com.spaceshipbunny.CouponSystem.beans;

import java.util.Collection;
import java.util.Objects;

/**
 * Java bean representing a customer
 * @author Yair Argaman
 */
public class Customer {
	private long id;
	private String custName;
	private String password;
	private Collection<Coupon> coupons;

	/**
	 * Empty constructor - initializes nothing
	 */
	public Customer() {
	}

	/**
	 * Full constructor
	 * @param id Customer ID
	 * @param custName Customer's name
	 * @param password Login password
	 * @param coupons Collection of all the customer's coupon
	 */
	public Customer(long id, String custName, String password, Collection<Coupon> coupons) {
		this.id = id;
		this.custName = custName;
		this.password = password;
		this.coupons = coupons;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<Coupon> getCoupons() {
		return coupons;
	}

	public void setCoupons(Collection<Coupon> coupons) {
		this.coupons = coupons;
	}

	/**
	 * Checks if the given object is equal in values and type to the customer object the method was called from.
	 * @param o The object to compare.
	 * @return <tt>true</tt> if the objects are equal in type and values.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Customer customer = (Customer) o;
		return getId() == customer.getId() &&
				Objects.equals(getCustName(), customer.getCustName()) &&
				Objects.equals(getPassword(), customer.getPassword()) &&
				Objects.equals(getCoupons(), customer.getCoupons());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getCustName(), getPassword(), getCoupons());
	}

	/**
	 * Returns string representation of the object.
	 * @return String representation of the object.
	 */
	@Override
	public String toString() {
		return "Customer{" +
				"id=" + id + ",\n" +
				"custName='" + custName + "',\n" +
				"password='" + password + "',\n" +
				"coupons=" + coupons + "\n" +
				'}';
	}
}
