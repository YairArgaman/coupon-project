package com.spaceshipbunny.CouponSystem.beans;

import java.util.Collection;
import java.util.Objects;

/**
 * Java bean representing a company
 * @author Yair Argaman
 */
public class Company {
	private long id;
	private String compName;
	private String password;
	private String email;
	private Collection<Coupon> coupons;

	/**
	 * Empty constructor - initializes nothing
	 */
	public Company() {
	}

	/**
	 * Full constructor
	 * @param id Company ID
	 * @param compName Company name
	 * @param password Login password
	 * @param email Company E-mail
	 * @param coupons Collection of all the company's coupon
	 */
	public Company(long id, String compName, String password, String email, Collection<Coupon> coupons) {
		this.id = id;
		this.compName = compName;
		this.password = password;
		this.email = email;
		this.coupons = coupons;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Collection<Coupon> getCoupons() {
		return coupons;
	}

	public void setCoupons(Collection<Coupon> coupons) {
		this.coupons = coupons;
	}

	/**
	 * Checks if the given object is equal in values and type to the company object the method was called from.
	 * @param o The object to compare.
	 * @return <tt>true</tt> if the objects are equal in type and values.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Company company = (Company) o;
		return getId() == company.getId() &&
				Objects.equals(getCompName(), company.getCompName()) &&
				Objects.equals(getPassword(), company.getPassword()) &&
				Objects.equals(getEmail(), company.getEmail()) &&
				Objects.equals(getCoupons(), company.getCoupons());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getCompName(), getPassword(), getEmail(), getCoupons());
	}

	/**
	 * Returns string representation of the object.
	 * @return String representation of the object.
	 */
	@Override
	public String toString() {
		return "Company{\n" +
				"\tid=" + id + "\n," +
				"compName='" + compName + "'\n," +
				" password='" + password + "'\n," +
				" email='" + email + "'\n," +
				" coupons=" + coupons + "\n" +
				'}';
	}
}
