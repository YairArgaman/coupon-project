package com.spaceshipbunny.CouponSystem.beans;

import java.io.Serializable;

/**
 * Enum representing the available coupon types
 * @author Yair Argaman
 */
public enum CouponType implements Serializable {
	ELECTRONICS,
	FOOD,
	HEALTH,
	SPORTS,
	CAMPING,
	TRAVELING,
	HOBBIES,
	ENTERTAINMENT; // Add more as needed
}