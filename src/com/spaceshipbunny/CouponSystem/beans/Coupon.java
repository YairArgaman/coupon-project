package com.spaceshipbunny.CouponSystem.beans;

import java.sql.Date;
import java.util.Objects;

/**
 * Java bean representing a coupon
 * @author Yair Argaman
 */
public class Coupon {
	private long id;
	private String title;
	private Date startDate;
	private Date endDate;
	private int amount;
	private CouponType type;
	private String message;
	private double price;
	private String image;

	/**
	 * Empty constructor - initializes nothing.
	 */
	public Coupon( ) {
	}

	/**
	 * Full constructor.
	 * @param id Coupon ID.
	 * @param title Coupon name - short descriptor of the coupon.
	 * @param startDate Coupon start date.
	 * @param endDate Coupon end date.
	 * @param amount Number of available coupons.
	 * @param type Coupon type - uses CouponType enum.
	 * @param message Full coupon description.
	 * @param price Coupon price.
	 * @param image Address or path of related image.
	 */
	public Coupon( long id, String title, Date startDate, Date endDate, int amount, CouponType type, String message, double price, String image ) {
		this.id = id;
		this.title = title;
		this.startDate = startDate;
		this.endDate = endDate;
		this.amount = amount;
		this.type = type;
		this.message = message;
		this.price = price;
		this.image = image;
	}

	/**
	 * Gets coupon's ID.
	 * @return Coupon's ID.
	 */
	public long getId( ) {
		return id;
	}

	/**
	 * Sets coupon's ID.
	 * @param id Coupon ID to set.
	 */
	public void setId( long id ) {
		this.id = id;
	}

	/**
	 * Gets coupon's title.
	 * @return Coupon's title.
	 */
	public String getTitle( ) {
		return title;
	}

	/**
	 * Sets coupon's title.
	 * @param title Coupon title to set.
	 */
	public void setTitle( String title ) {
		this.title = title;
	}

	/**
	 * Gets coupon's start date.
	 * @return Coupon's start date.
	 */
	public java.sql.Date getStartDate( ) {
		return startDate;
	}

	/**
	 * Sets coupon's start date.
	 * @param startDate Coupon start date to set.
	 */
	public void setStartDate( Date startDate ) {
		this.startDate = startDate;
	}

	/**
	 * Gets coupon's end date.
	 * @return Coupon's end date.
	 */
	public java.sql.Date getEndDate( ) {
		return endDate;
	}

	/**
	 * Sets coupon's end date.
	 * @param endDate Coupon's end date.
	 */
	public void setEndDate( Date endDate ) {
		this.endDate = endDate;
	}

	/**
	 * Gets the remaining quantity of the coupon.
	 * @return The remaining quantity of the coupon.
	 */
	public int getAmount( ) {
		return amount;
	}

	/**
	 * Sets the remaining quantity of the coupon.
	 * @param amount The remaining quantity of the coupon
	 */
	public void setAmount( int amount ) {
		this.amount = amount;
	}

	/**
	 * Gets the coupon's type.
	 * @return The coupon's type.
	 */
	public CouponType getType( ) {
		return type;
	}

	/**
	 * Sets the coupon's type.
	 * @param type The coupon's type.
	 */
	public void setType( CouponType type ) {
		this.type = type;
	}

	/**
	 * Gets the coupon's full description.
	 * @return The coupon's full description.
	 */
	public String getMessage( ) {
		return message;
	}

	/**
	 * Sets the coupon's full description.
	 * @param message The coupon's full description to set.
	 */
	public void setMessage( String message ) {
		this.message = message;
	}

	/**
	 * Gets the coupon's price.
	 * @return The coupon's price.
	 */
	public double getPrice( ) {
		return price;
	}

	/**
	 * Sets the coupon's price.
	 * @param price The coupon's price.
	 */
	public void setPrice( double price ) {
		this.price = price;
	}

	/**
	 * Gets the address or path of the related image.
	 * @return The address or path of the related image.
	 */
	public String getImage( ) {
		return image;
	}

	/**
	 * Sets the address or path of the related image.
	 * @param image The address or path of the related image.
	 */
	public void setImage( String image ) {
		this.image = image;
	}

	/**
	 * Checks if the given object is equal in values and type to the coupon object the method was called from.
	 * @param o The object to compare.
	 * @return <tt>true</tt> if the objects are equal in type and values.
	 */
	@Override
	public boolean equals( Object o ) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Coupon coupon = (Coupon) o;
		return getId() == coupon.getId() &&
				getAmount() == coupon.getAmount() &&
				Double.compare(coupon.getPrice(), getPrice()) == 0 &&
				Objects.equals(getTitle(), coupon.getTitle()) &&
				Objects.equals(getStartDate(), coupon.getStartDate()) &&
				Objects.equals(getEndDate(), coupon.getEndDate()) &&
				getType() == coupon.getType() &&
				Objects.equals(getMessage(), coupon.getMessage()) &&
				Objects.equals(getImage(), coupon.getImage());
	}

	@Override
	public int hashCode( ) {
		return Objects.hash(getId(), getTitle(), getStartDate(), getEndDate(), getAmount(), getType(), getMessage(), getPrice(), getImage());
	}

	/**
	 * Returns string representation of the object.
	 * @return String representation of the object.
	 */
	@Override
	public String toString( ) {
		return "Coupon{" + '\n' +
				"id=" + id + ",\n" +
				"title='" + title + "',\n" +
				"startDate=" + startDate + ",\n" +
				"endDate=" + endDate + ",\n" +
				"amount=" + amount + ",\n" +
				"type=" + type + ",\n" +
				"message='" + message + "',\n" +
				"price=" + price + ",\n" +
				"image='" + image + "'\n" +
				'}';
	}
}
