package com.spaceshipbunny.CouponSystem;

import com.spaceshipbunny.CouponSystem.exceptions.*;
import com.spaceshipbunny.CouponSystem.system.*;

import java.sql.Connection;
import java.sql.SQLException;

public class Test {
	ConnectionPool connectionPool = ConnectionPool.getInstance();

	public Test() {
		System.out.println("Running Test.main...");
		System.out.print("\t");
		System.out.println("\tTesting ConnectionPool...");
		try {
			testConnectionPool( );
		} catch ( TestException e ) {
			System.err.println( "Test failed! " + e.getCause() );
			e.printStackTrace( );
		}
	}

	public boolean testConnectionPool() throws TestException {
		boolean success = true;
		try {
			Connection connection = connectionPool.getConnection();
			if (connectionPool == null) {
				throw new TestException("Get connection failed. No connection pool");
			}
		} catch (DAOException | TestException e) {
			success = false;
			throw new TestException("ConnectionPool test failed! ", e);
		} finally {
			return success;
		}
	}
}