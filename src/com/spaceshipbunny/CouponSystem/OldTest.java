package com.spaceshipbunny.CouponSystem;

import java.sql.Date;
import com.spaceshipbunny.CouponSystem.beans.*;
import com.spaceshipbunny.CouponSystem.system.CouponSystem;
import com.spaceshipbunny.CouponSystem.system.CreateDB;
import com.spaceshipbunny.CouponSystem.exceptions.*;
import com.spaceshipbunny.CouponSystem.facade.*;

/**
 * Tests the implementation of the coupon system and it's classes.
 * @author Ron
 *
 */
public class OldTest {
	private static CouponSystem sys = CouponSystem.getInstance();
	private static AdminFacade adminFacade = null;
	private static CompanyFacade companyFacade = null;
	private static CustomerFacade customerFacade = null;
	private static CouponClientFacade couponClientFacade = null;

	/*private static String driverName = "org.apache.derby.jdbc.ClientDriver";
	private static String databaseUrl = "jdbc:derby://localHost:1527/coupon_system;create = true";
	private static String userName = null;
	private static String password = null;*/
	private static String driverName = "com.mysql.jdbc.Driver";
	private static String databaseUrl = "jdbc:mysql://db4free.net:3306/coupon_system";
	private static String userName = "coupon_group";
	private static String password = "12345678";

	public static void main(String[] args) {
		sys.setServer(driverName, databaseUrl, userName, password);
		testExceptions();
		try {
			createDefault();
		} catch (CouponSystemException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		sys.shutdown();
	}

	private static AdminFacade loginAdmin() throws CouponSystemException {
		couponClientFacade =  sys.login("admin", "1234", ClientType.ADMIN);
		System.out.println("LOG : Admin logged in");
		return (AdminFacade) couponClientFacade;
	}

	private static CompanyFacade loginCompany(String user, String password) throws CouponSystemException {
		couponClientFacade =  sys.login(user, password, ClientType.COMPANY);
		System.out.println("LOG : Company logged in");
		return (CompanyFacade) couponClientFacade;
	}

	private static CustomerFacade loginCustomer(String user, String password) throws CouponSystemException {
		couponClientFacade =  sys.login(user, password, ClientType.CUSTOMER);
		System.out.println("LOG : customer logged in");
		return (CustomerFacade) couponClientFacade;
	}

	/**
	 * Populates the DB with default values
	 *
	 * @throws CouponSystemException
	 */
	private static void createDefault() throws CouponSystemException {
		resetDB();
		adminFacade = loginAdmin();
		if(adminFacade==null)
			return;
		System.out.println("Creating default DB...");
		Customer customer = new Customer();
		Coupon coupon = new Coupon();
		Company company = new Company();
		for (char i = 97; i < 117; i++) {
			company.setId(100000 + i);
			company.setCompName(""+i+i+i+" "+i+i+i+i);
			company.setPassword(""+i+i+i+i+i+i);
			company.setEmail(""+i+i+i+"@"+i+i+i+i+".com");
			adminFacade.createCompany(company);
			System.out.println("LOG : Company created \n" + company);
			companyFacade = loginCompany(""+i+i+i+" "+i+i+i+i, ""+i+i+i+i+i+i);
			coupon.setId(200000 + i);
			coupon.setTitle(""+i+i+i+" "+i+i+i+i);
			coupon.setStartDate(new Date(System.currentTimeMillis()+(1000*60*60*24)));
			coupon.setEndDate(new Date(System.currentTimeMillis()+(1000*60*60*24*30*12)));
			coupon.setAmount(50);
			coupon.setType(CouponType.CAMPING);
			coupon.setMessage("aaaaaa");
			coupon.setPrice(200);
			coupon.setImage("aaaaaaaaaaaaaa");
			companyFacade.createCoupon(coupon);
			System.out.println("LOG : Coupon created \n" + coupon);
			customer.setId(100032 + i);
			customer.setCustName(""+i+i+i+" "+i+i+i+i);
			customer.setPassword(""+i+i+i+i+i+i);
			adminFacade.createCustomer(customer);

			customerFacade = loginCustomer(""+i+i+i+" "+i+i+i+i, ""+i+i+i+i+i+i);
			customerFacade.purchaseCoupon(coupon);
			System.out.println("LOG : Coupon purchased \n" + coupon);

		}
	}
	private static void testExceptions() {
		try {
			resetDB();
		} catch (CouponSystemException e1) {
			e1.printStackTrace();
		}
		try {
			adminFacade = loginAdmin();
		} catch (CouponSystemException e) {System.err.println(e.getMessage());}
		if(adminFacade==null)
			return;
		System.out.println("Running exception test...");
		Customer customer = new Customer();
		Coupon coupon = new Coupon();
		Company company = new Company();
		for (char i = 97; i < 117; i++) {
			company.setId(100000 + i);
			company.setCompName(""+i+i+i+" "+i+i+i+i);
			company.setPassword(""+i+i+i+i+i+i);
			company.setEmail(""+i+i+i+"@"+i+i+i+i+".com");
			coupon.setId(200000 + i);
			coupon.setTitle(""+i+i+i+" "+i+i+i+i);
			coupon.setStartDate(new Date(System.currentTimeMillis()+(1000*60*60*24)));
			coupon.setEndDate(new Date(System.currentTimeMillis()+(1000*60*60*24*30*12)));
			coupon.setAmount(50);
			coupon.setType(CouponType.CAMPING);
			coupon.setMessage("aaaaaa");
			coupon.setPrice(200);
			coupon.setImage("aaaaaaaaaaaaaa");
			customer.setId(100032 + i);
			customer.setCustName(""+i+i+i+" "+i+i+i+i);
			customer.setPassword(""+i+i+i+i+i+i);


			switch(i%16) {
				case 0:
					company.setId(100000 + i);
					break;
				case 1:
					company.setPassword(""+i+i+i+i);
					break;
				case 2:
					company.setEmail(""+i+i+i+"@"+i+i+i+i+"");
					break;
				case 3:
					coupon.setId(200000 + i);
					break;
				case 4:
					coupon.setTitle(""+i+i+i+" "+i+i+i+i +"ggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
					break;
				case 5:
					coupon.setStartDate(new Date(System.currentTimeMillis()-(1000*60*60*24)));
					break;
				case 6:
					coupon.setEndDate(new Date(System.currentTimeMillis()-(1000*60*60*24*30)));
					break;
				case 7:
					coupon.setAmount(-4);
					break;
				case 8:
					coupon.setType(null);
					break;
				case 9:
					coupon.setMessage("aaaaaa"+"ggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
					break;
				case 10:
					coupon.setPrice(-4);
					break;
				case 11:
					coupon.setImage("aaaaaaaaaaaaaa");
					break;
				case 12:
					customer.setId(100032 + i);
					break;
				case 13:
					customer.setCustName(""+i+i+i+" "+i+i+i+i+"ggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
					break;
				case 14:
					customer.setPassword(""+i+i+i+i+i+i+"ggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
					break;
				case 15:
					company.setCompName(""+i+i+i+i+i+i+"ggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
							+ "gggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
					break;
			}
			//TODO Add unauthorized update/remove + same coupon purchase


			try {
				adminFacade.createCompany(company);
				System.out.println("LOG : Company created \n" + company);

			} catch (AdminFacadeException e) {System.err.println(e);}
			try {
				companyFacade = loginCompany(""+i+i+i+" "+i+i+i+i, ""+i+i+i+i+i+i);
			} catch (CouponSystemException e) {System.err.println(e);}
			try {
				if(companyFacade != null) {
					companyFacade.createCoupon(coupon);
					System.out.println("LOG : Coupon created \n" + coupon);
				}
			} catch (CompanyFacadeException e) {System.err.println(e);}
			try {
				adminFacade.createCustomer(customer);
				System.out.println("LOG : Customer created \n" + customer);
			} catch (AdminFacadeException e) {System.err.println(e);}

			try {
				if(customerFacade != null)
					customerFacade = loginCustomer(""+i+i+i+" "+i+i+i+i, ""+i+i+i+i+i+i);
			} catch (CouponSystemException e) {System.err.println(e);}
			try {
				if(customerFacade != null) {
					customerFacade.purchaseCoupon(coupon);
					System.out.println("LOG : Coupon purchased \n" + coupon);

				}
			} catch (CustomerFacadeException e) {System.err.println(e);}
			try {
				if(companyFacade != null) {
					companyFacade.removeCoupon(coupon);
					System.out.println("LOG : Coupon deleted \n" + coupon);
				}
			} catch (CompanyFacadeException e) {System.err.println(e);}
			try {
				adminFacade.removeCustomer(customer);
				System.out.println("LOG : Customer deleted \n" + customer);

			} catch (AdminFacadeException e) {System.err.println(e);}
			try {
				adminFacade.removeCompany(company);
				System.out.println("LOG : Company deleted \n" + company);
			} catch (AdminFacadeException e) {System.err.println(e);}
			//TODO add all facade methods
		}
	}

	/**
	 * Resets the DB from scratch.
	 * @throws DAOException
	 */
	public static void resetDB() throws CouponSystemException {
		CreateDB db = new CreateDB();
		db.dropTables();
		db.createDb();
	}

}
