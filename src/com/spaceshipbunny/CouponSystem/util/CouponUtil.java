package com.spaceshipbunny.CouponSystem.util;

import java.sql.Date;

import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.beans.CouponType;
import com.spaceshipbunny.CouponSystem.exceptions.CouponException;

/**
 * Utility class used to check validity (length and/or format) of Coupon's String properties (checkCoupon calls all other methods in utility class)
 * @author Ron
 *
 */
public class CouponUtil implements DataUtil{

	private static final long serialVersionUID = 1L;

	/**
	 * Private constructor
	 */
	private CouponUtil() {
		// TODO Auto-generated constructor stub
	}

	public static void checkCoupon(Coupon coupon) throws CouponException {
		if (!checkTitle(coupon.getTitle()))
			throw new CouponException("Coupon's title can't be longer than " + COUP_TITLE_LENGTH + " characters");
		if (!checkStartDate(coupon.getStartDate()))
			throw new CouponException("Coupon's start date can't be earlier than today");
		if (!checkEndDate(coupon.getEndDate()))
			throw new CouponException("Coupon's expiration date can't be earlier than today");
		if (!checkAmount(coupon.getAmount()))
			throw new CouponException("Coupon's amount can't be negative");
		if (!checkType(coupon.getType()))
			throw new CouponException("Coupon title cant be more than " + COUP_TITLE_LENGTH + " characters");
		if (!checkPrice(coupon.getPrice()))
			throw new CouponException("Coupon's price can't be negative");
		if (!checkImage(coupon.getImage()))
			throw new CouponException("Coupon image cant be ... ");//No restrictions at the momment
		if (!checkMessage(coupon.getMessage()))
			throw new CouponException("Coupon's message can't be longer than " + COUP_MSG_LENGTH + " characters");

	}

	/*public static boolean checkId(long id) {
		return true;
	}*/

	public static boolean checkTitle(String title) {
		if(title.length()>COUP_TITLE_LENGTH)
			return false;
		return true;
	}

	public static boolean checkStartDate(Date startDate) {
		if(startDate.before(new java.util.Date(System.currentTimeMillis())))
			return false;
		return true;
	}

	public static boolean checkEndDate(Date endDate) {
		if(endDate.before(new java.util.Date(System.currentTimeMillis())))
			return false;
		return true;
	}

	public static boolean checkAmount(int amount) {
		if(amount<0)
			return false;
		return true;
	}

	public static boolean checkType(CouponType type) {
		if(type == null)
			return false;
		return true;
	}

	public static boolean checkPrice(double price) {
		if(price<0)
			return false;
		return true;
	}

	public static boolean checkImage(String image) {
		return true;
	}

	public static boolean checkMessage(String message) {
		if(message.length()>COUP_MSG_LENGTH)
			return false;
		return true;
	}
}
