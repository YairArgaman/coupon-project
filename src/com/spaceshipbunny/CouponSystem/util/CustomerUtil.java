package com.spaceshipbunny.CouponSystem.util;

import com.spaceshipbunny.CouponSystem.beans.Customer;
import com.spaceshipbunny.CouponSystem.exceptions.CustomerException;

/**
 * Utility class used to check validity (length and/or format) of Customer's String properties (checkCustomer calls all other methods in utility class)
 * @author Yair
 *
 */
public class CustomerUtil implements DataUtil{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Private constructor
	 */
	private CustomerUtil() {
		// TODO Auto-generated constructor stub
	}

	public static void checkCustomer(Customer customer) throws CustomerException {

		// check if customer name is not too long
		if (!checkCustomerName(customer.getCustName())) {
			CustomerException e = new CustomerException(
					"The customer name cant be more than " + CUST_NAME_LENGTH + " characters");
			throw e;
		}

		// check if customer password is not too long
		if (!checkCustomerPassword(customer.getPassword())) {
			CustomerException e = new CustomerException(
					"The customer password cant be more than " + CUST_PASSWORD_LENGTH + " characters");
			throw e;
		}
		// check if customer password is not too short
		if (!checkCustomerPasswordNotShort(customer.getPassword())) {
			CustomerException e = new CustomerException(
					"The customer password need to be more than " + CUST_PASSWORD_MIN_LENGTH + " characters");
			throw e;
		}

	}

	private static boolean checkCustomerPasswordNotShort(String password) {
		if (password.length() < CUST_PASSWORD_MIN_LENGTH) {
			return false;
		}
		return true;
	}

	private static boolean checkCustomerName(String custName) {
		if (custName.length() > CUST_NAME_LENGTH) {
			return false;
		}
		return true;
	}

	private static boolean checkCustomerPassword(String password) {
		if (password.length() > CUST_PASSWORD_LENGTH) {
			return false;
		}
		return true;
	}



}
