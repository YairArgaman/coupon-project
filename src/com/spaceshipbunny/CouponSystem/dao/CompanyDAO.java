package com.spaceshipbunny.CouponSystem.dao;

import java.io.Serializable;
import java.util.Collection;
import com.spaceshipbunny.CouponSystem.beans.Company;
import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.exceptions.CompanyException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;

/**
 * An interface for a DAO class which accesses a {@link Company} type DAO.
 * @author Hagai
 */
public interface CompanyDAO extends Serializable{

	/**
	 * Attempts to create a given company in the DB
	 *
	 * @param company The company to create
	 * @throws DAOException If there is a connection problem or SQLException.
	 * @throws CompanyException If insertion of the given company to the DB fails (e.g. Company ID already exists).
	 *
	 */
	public void createCompany(Company company) throws DAOException, CompanyException;

	/**
	 * Removes a specified company from the DB.
	 *
	 * @param company The company to be removed.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CompanyException If the given company's ID can't be found in the DB.
	 *
	 */
	public void removeCompany(Company company) throws DAOException, CompanyException;

	/**
	 * Updates all of a company's fields (except ID) in the DB according to the given company bean.
	 *
	 * @param company The company to be updated
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CompanyException If the given company's ID can't be found in the DB (0 rows were updated).
	 */
	public void updateCompany(Company company) throws DAOException, CompanyException;

	/**
	 * Searches the DB for a company with the given ID and
	 * returns a Company bean with it's data from the DB.
	 *
	 * @param id The id of the company to find in the DB.
	 * @return {@link Company} bean; <code>null</code> - if no company with the given ID exists in DB
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CompanyException If the given company's ID can't be found in the DB (0 rows were returned).
	 */
	public Company getCompany(long id) throws DAOException, CompanyException;

	/**
	 * Searches the DB for a company with the given name and
	 * returns a {@link Company} bean with it's data from the DB.
	 *
	 * @param name The name of company to find in the DB.
	 * @return {@link Company} bean; <code>null</code> - if no company with the given ID exists in DB
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CompanyException If the given company's ID can't be found in the DB (0 rows were returned).
	 */
	public Company getCompanyByName(String name) throws DAOException, CompanyException;

	/**
	 * Assemble and return an <code>ArrayList</code> of all the companies in the DB.
	 *
	 * @return An <code>ArrayList</code> of all the companies in DB.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 */
	public Collection<Company> getAllCompanies() throws DAOException;

	/**
	 * Assemble and return an <code>ArrayList</code> of all the coupons of
	 * a given company from the DB.
	 *
	 * @param companyId The ID of the company whose coupons we want to get.
	 * @return A populated <code>ArrayList</code> of all the coupons; An empty <code>ArrayList</code>
	 * if there is none).
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 */
	public Collection<Coupon> getCoupons(long companyId) throws DAOException;
}
