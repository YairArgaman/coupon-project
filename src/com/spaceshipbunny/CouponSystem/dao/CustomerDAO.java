package com.spaceshipbunny.CouponSystem.dao;

import java.io.Serializable;
import java.util.Collection;

import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.beans.Customer;
import com.spaceshipbunny.CouponSystem.exceptions.CustomerException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;

/**
 * An interface for a DAO class which accesses a {@link Customer} type DAO.
 *
 * @author Ron
 *
 */
public interface CustomerDAO extends Serializable {
	/**
	 * Attempts to create a given customer in the DB
	 *
	 *
	 * @param customer The customer to create
	 * @throws DAOException If there is a connection problem or <code>SQLException</code>.
	 * @throws CustomerException If insertion of the given customer to the DB fails (e.g. <code>Customer</code> ID already exists).
	 *
	 */
	public void createCustomer(Customer customer) throws DAOException, CustomerException;

	/**
	 * Removes a specified customer from the DB.
	 *
	 * @param customer The customer to be removed.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CustomerException If the given customer's ID can't be found in the DB.
	 *
	 */
	public void removeCustomer(Customer customer) throws DAOException, CustomerException;

	/**
	 * Updates all of a customer's fields (except ID) in the DB according to the given customer bean.
	 *
	 * @param customer The customer to be updated
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CustomerException If the given customer's ID can't be found in the DB (0 rows were updated).
	 */
	public void updateCustomer(Customer customer) throws DAOException, CustomerException;

	/**
	 * Searches the DB for a customer with the given ID and
	 * returns a Customer bean with it's data from the DB.
	 *
	 * @param custID The id of the customer to find in the DB.
	 * @return {@link Customer} bean; <code>null</code> - if no customer with the given ID exists in DB
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CustomerException If the given customer's ID can't be found in the DB (0 rows were returned).
	 */
	public Customer getCustomer(long custID) throws DAOException, CustomerException;

	/**
	 * Searches the DB for a customer with the given name and
	 * returns a {@link Customer} bean with it's data from the DB.
	 *
	 * @param custName The name of customer to find in the DB.
	 * @return {@link Customer} bean; <code>null</code> - if no customer with the given ID exists in DB
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CustomerException If the given customer's ID can't be found in the DB (0 rows were returned).
	 */
	public Customer getCustomerByName(String custName) throws DAOException, CustomerException;

	/**
	 * Assemble and return an <code>ArrayList</code> of all the coupons of
	 * a given customer from the DB.
	 *
	 * @param customer The ID of the customer whose coupons we want to get.
	 * @return A populated <code>ArrayList</code> of all the coupons; An empty <code>ArrayList</code>
	 * if there is none).
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 */
	public Collection<Coupon> getCoupons(Customer customer) throws DAOException;

	/**
	 * Assemble and return an <code>ArrayList</code> of all the companies in the DB.
	 *
	 * @return An <code>ArrayList</code> of all the companies in DB.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 */
	public Collection<Customer> getAllCustomers() throws DAOException;
}
