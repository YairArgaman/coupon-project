package com.spaceshipbunny.CouponSystem.dao;

import java.io.Serializable;
import java.util.Collection;

import com.spaceshipbunny.CouponSystem.beans.*;
import com.spaceshipbunny.CouponSystem.beans.CouponType;
import com.spaceshipbunny.CouponSystem.exceptions.CouponException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;

/**
 * An interface for a DAO class which accesses a {@link Coupon} type DAO.
 *
 * @author Ron
 *
 */
public interface CouponDAO extends Serializable{

	/**
	 * Adds a new {@link Coupon} to coupon table
	 *
	 * @param coupon The new {@link Coupon} to be added.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If {@link Coupon}  could not be added
	 */
	public void createCoupon(Coupon coupon) throws DAOException, CouponException;
	/**
	 * Removes a {@link Coupon} from coupon table
	 *
	 * @param coupon The {@link Coupon}  to be removed.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If {@link Coupon}  could not be removed
	 */
	public void removeCoupon(Coupon coupon) throws DAOException, CouponException;

	/**
	 * Updates a specific {@link Coupon} in the DB.
	 *
	 * @param coupon The {@link Coupon}  to be updated.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If {@link Coupon}  could not be updated
	 */
	public void updateCoupon(Coupon coupon) throws DAOException, CouponException;

	/**
	 * Adds a {@link Coupon} to the {@link Customer} and updates the amount.
	 * @param coupon The {@link Coupon} to purchase.
	 * @param custId The ID of the {@link Customer} purchasing the coupon.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If coupon could not be purchased
	 */
	public void purchaseCoupon(Coupon coupon, long custId) throws DAOException, CouponException;

	/**
	 * Fetches a specific {@link Coupon} from the DB using its ID.
	 *
	 * @param couponID The ID of the desired {@link Coupon}.
	 * @return The coupon that matches the ID; <br>
	 * 		   <code>null</code> if there is no match.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If could not get coupon
	 */
	public Coupon getCoupon(long couponID) throws DAOException, CouponException;

	/**
	 * Fetches a specific {@link Coupon} from the DB using its title.
	 *
	 * @param title The title of the desired {@link Coupon}.
	 * @return The {@link Coupon}  that matches the title; <br>
	 * 		   <code>null</code> if there is no match.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If could not get coupon
	 */
	public Coupon getCouponByTitle(String title) throws DAOException, CouponException;

	/**
	 * Fetches and assembles all the coupons in a {@link Collection}.
	 *
	 * @return A {@link Collection} of all coupons.
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 */
	public Collection<Coupon> getAllCoupons() throws DAOException;

	/**
	 * Gets all coupons of the given CouponType
	 * @param type The Type of coupons desired.
	 * @return a Collection of all coupons with matching a Type
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 */
	public Collection<Coupon> getCouponsByType(CouponType type) throws DAOException;

	/**
	 * Adds a coupon to a Company ({@link Company}).
	 *
	 * @param coupon The coupon to be added
	 * @param compId The company's Id
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If could not add coupon to company
	 */
	public void addCouponToCompany(Coupon coupon, long compId) throws DAOException, CouponException;

	/**
	 * Removes a coupon from a Company ({@link Company}).
	 *
	 * @param coupon the coupon to be removed
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If could not remove coupon from company
	 */
	public void removeCouponFromCompanies(Coupon coupon) throws DAOException, CouponException;

	/**
	 * Removes a coupon from a Customer ({@link Customer}).
	 *
	 * @param coupon The coupon to be removed
	 * @param custId The customer's Id
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If could not remove coupon from customer
	 */
	public void removeCouponFromCustomer(Coupon coupon, long custId) throws DAOException, CouponException;

	/**
	 * Removes a coupon from all Customers ({@link Customer}).
	 *
	 * @param coupon The coupon to be removed
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 * @throws CouponException If could not remove coupon from customers
	 */
	public void removeCouponFromCustomers(Coupon coupon) throws DAOException, CouponException;

}
