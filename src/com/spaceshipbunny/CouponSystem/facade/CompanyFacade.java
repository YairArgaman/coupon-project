package com.spaceshipbunny.CouponSystem.facade;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Collection;
import com.spaceshipbunny.CouponSystem.beans.Company;
import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.beans.CouponType;
import com.spaceshipbunny.CouponSystem.dao.CompanyDAO;
import com.spaceshipbunny.CouponSystem.dao.CouponDAO;
import com.spaceshipbunny.CouponSystem.dbdao.CompanyDBDAO;
import com.spaceshipbunny.CouponSystem.dbdao.CouponDBDAO;
import com.spaceshipbunny.CouponSystem.exceptions.CompanyException;
import com.spaceshipbunny.CouponSystem.exceptions.CompanyFacadeException;
import com.spaceshipbunny.CouponSystem.exceptions.CouponException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;
import com.spaceshipbunny.CouponSystem.util.CouponUtil;

/**
 * Facade used to access the coupon system by Companies
 * @author Hagai
 *
 */
public class CompanyFacade implements CouponClientFacade {
	private static final long serialVersionUID = 1L;
	private static final CouponDAO couponDAO = CouponDBDAO.getInstance();
	private static final CompanyDAO companyDAO = CompanyDBDAO.getInstance();
	private final Company company;

	/**
	 * Public constructor initializes given company's access
	 * @param company Company to access in the system
	 */
	private CompanyFacade(Company company) {
		super();
		this.company = company;
	}

	/**
	 * Adds a new {@link Coupon} to the DB in the following order:
	 * <ul>
	 * <li>To Table <code>coupon</code></li>
	 * <li>To Table <code>comp_coupon</code></li>
	 * </ul>
	 *
	 * @param coupon The new {@link Coupon} to be added.
	 * @throws CompanyFacadeException if operation was unsuccessful
	 */
	public void createCoupon(Coupon coupon) throws CompanyFacadeException {
		try {
			coupon.setId(coupon);
		} catch (IdGeneratorException e) {
			throw new CompanyFacadeException("Can't create new coupon! ", e);
		}

		try {
			// Check if coupon ID is unique in DB
			couponDAO.getCoupon(coupon.getId());
			throw new CompanyFacadeException( "Can't create new coupon! Generated ID is already in DB. " );

		} catch (DAOException e) {
			throw new CompanyFacadeException( "Can't create new coupon! ", e);
		} catch (CouponException e) {
			// No coupon was found can proceed
		}

		try {
			// Check if coupon title is unique in DB
			couponDAO.getCouponByTitle(coupon.getTitle());
			throw new CompanyFacadeException(
					"Can't create new coupon, the title is already in DB");

		} catch (DAOException e) {
			throw new CompanyFacadeException("Can't create new coupon", e);
		} catch (CouponException e) {
			// No coupon was found; can proceed
		}

		try {
			// check if coupon bean is legal data
			CouponUtil.checkCoupon(coupon);
			couponDAO.createCoupon(coupon);
			couponDAO.addCouponToCompany(coupon, company.getId());
		} catch (DAOException | CouponException e) {
			throw new CompanyFacadeException("Can't create new coupon : \n" + e.getMessage(), e);
		}

	}

	/**
	 * Removes a {@link Coupon} to the DB in the following order:
	 * <ul>
	 * <li> From Table <code>cust_coupon</code> </li>
	 * <li> From Table <code>comp_coupon</code> </li>
	 * <li> From Table <code>coupon</code> </li>
	 * </ul>
	 *
	 * @param coupon The coupon to be removed.
	 * @throws CompanyFacadeException if operation was unsuccessful
	 */
	public void removeCoupon(Coupon coupon) throws CompanyFacadeException {
		//check if the coupon exists
		// if this company is authorize to remove the coupon.
		getCoupon(coupon.getId());
		this.checkIfCompanyAuthorize(coupon);
		try {
			couponDAO.removeCouponFromCompanies(coupon);
			// remove coupon from customer_coupon table ;
			couponDAO.removeCouponFromCustomers(coupon);
			// remove coupon from coupon table ;
			couponDAO.removeCoupon(coupon);
		} catch (DAOException | CouponException e) {
			throw new CompanyFacadeException("Can't remove coupon. ", e);
		}
	}


	/**
	 * Updates a specific {@link Coupon} in the DB.
	 *
	 * @param clientCoupon The coupon to be updated.
	 * @throws CompanyFacadeException if operation was unsuccessful
	 */
	public void updateCoupon(Coupon clientCoupon) throws CompanyFacadeException {
		// check if this company is authorize to update the coupon. and get the coupon
		// fields from DB
		Coupon coupToUpdate = getCoupon(clientCoupon.getId());

		// check if clientCoupon has legal data
		try {
			CouponUtil.checkCoupon(clientCoupon);
			// use the coupon data from DB and change the fields that we want to update (end
			// date and price)
			coupToUpdate.setEndDate(clientCoupon.getEndDate());
			coupToUpdate.setPrice(clientCoupon.getPrice());
			couponDAO.updateCoupon(coupToUpdate);
		} catch (CouponException | DAOException e) {
			throw new CompanyFacadeException("Can't update new coupon: \n" + e.getMessage(), e);
		}


	}

	/**
	 * Fetches a specific {@link Coupon} from the DB using its ID.
	 *
	 * @param couponID The ID of the desired {@link Coupon}.
	 * @return The coupon that matches the ID; <br>
	 * 		   <code>null</code> if there is no match.
	 * @throws CompanyFacadeException if operation was unsuccessful
	 */
	public Coupon getCoupon(long couponID) throws CompanyFacadeException {

		try {
			Coupon coup = couponDAO.getCoupon(couponID);
			// check if this company is authorize to get the coupon.
			this.checkIfCompanyAuthorize(coup);
			return coup;
		} catch (DAOException | CouponException e) {
			throw new CompanyFacadeException("Can't get coupon data from DB. ", e);
		}

	}

	/**
	 * Fetches and assembles all the coupons in a {@link Collection}.
	 *
	 * @return A {@link Collection} of all coupons.
	 * @throws CompanyFacadeException if operation was unsuccessful
	 */
	public Collection<Coupon> getAllCoupons() throws CompanyFacadeException {
		try {
			return companyDAO.getCoupons(company.getId());
		} catch (DAOException e) {
			throw new CompanyFacadeException("Can't get all coupons from DB. ", e);
		}

	}

	/**
	 * Gets all coupons of the given CouponType
	 * @param couponType The Type of coupons desired.
	 * @return a Collection of all coupons with matching a Type
	 * @throws CompanyFacadeException if operation was unsuccessful
	 */
	public Collection<Coupon> getCouponsByType(CouponType couponType) throws CompanyFacadeException {
		Collection<Coupon> coupons = this.getAllCoupons();
		for (Coupon coupon : coupons) {
			if (coupon.getType() != couponType) {
				coupons.remove(coupon);
			}
		}
		return coupons;
	}

	/**
	 * Logs in to the coupon system as a specific company.
	 * @param name Company username
	 * @param password Company password
	 * @return a new CompanyFacade instance if company's username and password are correct; otherwise, throws {@link CompanyFacade}
	 * @throws CompanyFacadeException if username or password are incorrect
	 */
	public static CompanyFacade login(String name, String password) throws CompanyFacadeException {
		try {
			if (CompanyDBDAO.login(name, password)) {
				return new CompanyFacade(companyDAO.getCompanyByName(name));
			}
		} catch (DAOException | CompanyException e) {
			throw new CompanyFacadeException("Can't login: \n" + e.getMessage(), e);
		}
		throw new CompanyFacadeException("User name or password are incorrect. ");
	}

	/**
	 * Checks if the company is authorized to access a given coupon.
	 * @param coupon The desired coupon to access
	 * @throws CompanyFacadeException if operation was unsuccessful
	 */
	private void checkIfCompanyAuthorize(Coupon coupon) throws CompanyFacadeException {
		if (!this.getAllCoupons().contains(coupon)) {
			throw new CompanyFacadeException("Company is not authorized to access given coupon. ");
		}
	}

}
