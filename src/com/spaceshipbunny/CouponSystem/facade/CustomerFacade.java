package com.spaceshipbunny.CouponSystem.facade;

import java.util.Collection;

import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.beans.CouponType;
import com.spaceshipbunny.CouponSystem.beans.Customer;
import com.spaceshipbunny.CouponSystem.dao.CouponDAO;
import com.spaceshipbunny.CouponSystem.dao.CustomerDAO;
import com.spaceshipbunny.CouponSystem.dbdao.CouponDBDAO;
import com.spaceshipbunny.CouponSystem.dbdao.CustomerDBDAO;
import com.spaceshipbunny.CouponSystem.exceptions.CouponException;
import com.spaceshipbunny.CouponSystem.exceptions.CustomerException;
import com.spaceshipbunny.CouponSystem.exceptions.CustomerFacadeException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;

/**
 * Facade used to access the coupon system by Customers
 * @author Yair
 *
 */
public class CustomerFacade implements CouponClientFacade {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static final CustomerDAO customerDAO = CustomerDBDAO.getInstance();
	private static final CouponDAO couponDAO = CouponDBDAO.getInstance();
	private final Customer customer;

	/**
	 * Private constructor that initializes given customer's access
	 * @param customer The customer that is accessing the system
	 *
	 * @return CustomerFacadeException
	 */
	private CustomerFacade(Customer customer) {
		// TODO Auto-generated constructor stub
		this.customer = customer;
	}

	/**
	 * Logs in to the coupon system as a specific company.
	 * @param custName Customer username
	 * @param password Customer password
	 * @return a new CustomerFacade instance if customer's username and password are correct; otherwise, throws {@link CustomerFacade}
	 * @throws CustomerFacadeException if username or password are incorrect
	 */
	//@Override
	public static CouponClientFacade login(String custName, String password) throws  CustomerFacadeException {
		try {
			if(CustomerDBDAO.login(custName, password)) {
				return new CustomerFacade(customerDAO.getCustomerByName(custName));
			}
			throw new CustomerFacadeException("Customer login failed : incorrect user name and/or password ");
		} catch (DAOException | CustomerException e) {
			throw new CustomerFacadeException("Customer login failed :\n" + e.getMessage(), e);
		}
	}

	/**
	 * Purchases the given coupon, adds it to the customer and
	 * updates the coupons's amount.
	 * -checks if customer owns this coupon
	 *
	 * @param coupon Coupon to purchase
	 * @throws CustomerFacadeException If coupon is out of stock or expired
	 * @throws CustomerFacadeException If coupon purchase fails
	 */
	public void purchaseCoupon(Coupon coupon) throws CustomerFacadeException {
		try {
			//checks if customer already owns this coupon
			coupon = couponDAO.getCoupon(coupon.getId());
			if(customerDAO.getCoupons(customer).contains(coupon))
				throw new CustomerFacadeException("You already own this coupon");

			if(coupon.getAmount() < 1 || coupon.getEndDate().getTime() < System.currentTimeMillis()) {
				throw new CustomerFacadeException("Coupon purchase failed : coupon is expired");
			}

			couponDAO.purchaseCoupon(coupon, customer.getId());
		} catch (DAOException | CouponException e) {
			throw new CustomerFacadeException("Coupon purchase failed :\n" + e.getMessage(), e);
		}
	}

	/**
	 * Gets all coupons purchased by the customer
	 * @return Collection of Coupons associated with the Customer
	 * @throws CustomerFacadeException If retrieval of coupons fails
	 */
	public Collection<Coupon> getAllPurchasedCoupons() throws CustomerFacadeException {
		try {
			return  customerDAO.getCoupons(customer);
		} catch (DAOException e) {
			throw new CustomerFacadeException("Get purchased Coupons failed :\n" + e, e);
		}
	}

	/**
	 * Gets all coupons of a specific CouponType that were purchased by the customer
	 *
	 * @param type the type of coupons to select
	 * @return Collection of Coupons associated with the Customer
	 * @throws CustomerFacadeException If retrieval of coupons fails
	 */
	public Collection<Coupon> getAllPurchasedCouponsByType(CouponType type) throws CustomerFacadeException {
		Collection<Coupon> coupons = getAllPurchasedCoupons();
		for (Coupon coupon : coupons) {
			if(!coupon.getType().equals(type)) {
				coupons.remove(coupon);
			}
		}
		return coupons;
	}

	/**
	 * Gets all coupons up to a certain price that were purchased by the customer
	 *
	 * @param price the max price of the coupons to select
	 * @return Collection of Coupons associated with the Customer
	 * @throws CustomerFacadeException If retrieval of coupons fails
	 */
	public Collection<Coupon> getAllPurchasedCouponsByPrice(double price) throws CustomerFacadeException {
		Collection<Coupon> coupons = getAllPurchasedCoupons();
		for (Coupon coupon : coupons) {
			if(coupon.getPrice() > price) {
				coupons.remove(coupon);
			}
		}
		return coupons;
	}
	/**
	 * Returns all available coupons
	 * @return Collection of Coupons
	 * @throws CustomerFacadeException If retrieval of coupons fails
	 */
	public Collection<Coupon> getAllCoupons() throws CustomerFacadeException {
		Collection<Coupon> coupons;
		try {
			coupons = couponDAO.getAllCoupons();
		} catch (DAOException e) {
			throw new CustomerFacadeException("Could not retrieve coupons", e);
		}
		return coupons;
	}

}
