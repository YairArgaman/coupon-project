package com.spaceshipbunny.CouponSystem.facade;

import java.io.Serializable;

/**
 * Interface used to represent a Facade to access the coupon system for various types of users (Admin, Company & Client)
 *
 * @author Ron
 *
 */
public interface CouponClientFacade extends Serializable{

}
