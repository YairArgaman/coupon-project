package com.spaceshipbunny.CouponSystem.facade;

import java.util.Collection;
import com.spaceshipbunny.CouponSystem.beans.Company;
import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.beans.Customer;
import com.spaceshipbunny.CouponSystem.dao.CompanyDAO;
import com.spaceshipbunny.CouponSystem.dao.CouponDAO;
import com.spaceshipbunny.CouponSystem.dao.CustomerDAO;
import com.spaceshipbunny.CouponSystem.dbdao.CompanyDBDAO;
import com.spaceshipbunny.CouponSystem.dbdao.CouponDBDAO;
import com.spaceshipbunny.CouponSystem.dbdao.CustomerDBDAO;
import com.spaceshipbunny.CouponSystem.exceptions.AdminFacadeException;
import com.spaceshipbunny.CouponSystem.exceptions.CompanyException;
import com.spaceshipbunny.CouponSystem.exceptions.CouponException;
import com.spaceshipbunny.CouponSystem.exceptions.CustomerException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;
import com.spaceshipbunny.CouponSystem.exceptions.IdGeneratorException;
import com.spaceshipbunny.CouponSystem.util.CompanyUtil;
import com.spaceshipbunny.CouponSystem.util.CustomerUtil;

/**
 * Facade used to access the coupon system by Administrators
 *
 * private constructor, instance is received thru static method login()
 *
 * @author Ron
 *
 */
public class AdminFacade implements CouponClientFacade{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final CompanyDAO companyDAO = CompanyDBDAO.getInstance();
	private final CustomerDAO customerDAO = CustomerDBDAO.getInstance();
	private final CouponDAO couponDAO = CouponDBDAO.getInstance();
//	private final int RETRIES = 3;

	/**
	 * Private constructor
	 */
	private AdminFacade() {
	}

	/**
	 * Logs in to the coupon system as an admin.
	 * @param name Admin user name
	 * @param password Admin password
	 * @return a new AdminFacade instance if admin's user name and password are correct; otherwise, throws {@link AdminFacade}
	 * @throws AdminFacadeException if user name or password are incorrect
	 */
	public static CouponClientFacade login(String name, String password) throws AdminFacadeException {
		if(name.equals("admin")&&password.equals("1234")) {
			return new AdminFacade();
		}else
			throw new AdminFacadeException("Admin : incorrect user name and/or password ");
	}

	/**
	 * Creates a new company in the database
	 * @param company Company to add to the DB
	 * @throws AdminFacadeException
	 *  If company name or ID already exist in the DB
	 *  If company creation fails
	 */
	public void createCompany(Company company) throws AdminFacadeException {
		try {
			CompanyUtil.checkCompany(company);

			if(companyNameExists(company)) {
				throw new AdminFacadeException("Company Name already exists");
			}

			company.setId(IdGenerator.generatCompanyId());

			if(companyIdExists(company)) {
				throw new AdminFacadeException("Company ID already exists");
			}
			companyDAO.createCompany(company);
		} catch (IdGeneratorException | CompanyException | DAOException e) {
			throw new AdminFacadeException("Create company failed:\n" + e.getMessage(), e);
		}

	}

	/**
	 * Deletes a company in the database
	 * -removes its coupons from DB (all tables)
	 * @param company Company to remove from the DB
	 * @throws AdminFacadeException
	 *  If given company's details don't match the details of the company with the same ID in the DB
	 *  If company deletion fails
	 */
	public void removeCompany(Company company) throws AdminFacadeException {
		try {
			if(!company.equals(getCompany(company.getId()))) {
				throw new AdminFacadeException("Company " + company.getId() + " details don't match");
			}
			Collection<Coupon> compCoupons;
			compCoupons = companyDAO.getCoupons(company.getId());
			for (Coupon coupon : compCoupons) {
				couponDAO.removeCouponFromCustomers(coupon);
				couponDAO.removeCouponFromCompanies(coupon);
				couponDAO.removeCoupon(coupon);
			}
			companyDAO.removeCompany(company);
		} catch (DAOException | CompanyException | CouponException e) {
			throw new AdminFacadeException("Remove company failed:\n" + e.getMessage(), e);
		}
	}

	/**
	 * Updates a company's details in the database
	 * @param company Company to remove from the DB
	 * @throws AdminFacadeException
	 *  If given company's details don't match the details of the company with the same ID in the DB
	 *  If updating of the company's details fails
	 */
	public void updateCompany(Company company) throws AdminFacadeException {
		try {
			CompanyUtil.checkCompany(company);

			Company tmpCompany = getCompany(company.getId());
			tmpCompany.setEmail(company.getEmail());
			tmpCompany.setPassword(company.getPassword());

			companyDAO.updateCompany(tmpCompany);
		} catch (CompanyException | DAOException e) {
			throw new AdminFacadeException("Update company failed:\n" + e.getMessage(), e);
		}
	}

	/**
	 * Gets a company's details from the database
	 *
	 * @param compID ID of company to retrieve from the DB
	 * @return Company the company with the matching id
	 * @throws AdminFacadeException
	 *  If there is a connection problem or an SQLException is thrown to the DAO.
	 *  If the given company's ID can't be found in the DB (0 rows were returned).
	 */
	public Company getCompany(long compID) throws AdminFacadeException {
		try {
			return companyDAO.getCompany(compID);
		} catch (DAOException | CompanyException e) {
			// TODO Auto-generated catch block
			throw new AdminFacadeException("Get company failed:\n" + e.getMessage(), e);
		}
	}

	/**
	 * Assemble and return an <code>ArrayList</code> of all the companies in the DB.
	 *
	 * @return An <code>ArrayList</code> of all the companies in DB.
	 * @throws AdminFacadeException If there is a connection problem or an <code>SQLException</code> is thrown to the DAO.
	 */
	public Collection<Company> getAllCompanies() throws AdminFacadeException{
		try {
			return companyDAO.getAllCompanies();
		} catch (DAOException e) {
			// TODO Auto-generated catch block
			throw new AdminFacadeException("Get companies failed:\n" + e.getMessage(), e);
		}
	}

	/**
	 * Attempts to create a given customer in the DB
	 *
	 * @param customer The customer to create
	 * @throws AdminFacadeException
	 *  If there is a connection problem or <code>SQLException</code> is thrown to the DAO.
	 *  If insertion of the given customer to the DB fails (e.g. <code>Customer</code> ID already exists or is invalid).
	 *
	 */
	public void createCustomer(Customer customer) throws AdminFacadeException {
		try {
			CustomerUtil.checkCustomer(customer);

			if(customerNameExists(customer)) {
				throw new AdminFacadeException("Customer Name already exists");
			}
			customer.setId(IdGenerator.generatCustomerId());

			if(customerIdExists(customer)) {
				throw new AdminFacadeException("Customer ID already exists");
			}
			customerDAO.createCustomer(customer);
		} catch (IdGeneratorException | CustomerException | DAOException e) {
			throw new AdminFacadeException("Create customer failed:\n" + e.getMessage(), e);
		}
	}

	/**
	 * Deletes a specified customer from the DB.
	 * -removes its coupons from the DB (customer_coupon table)
	 *
	 * @param customer The customer to be removed.
	 * @throws AdminFacadeException
	 *  If there is a connection problem or an <code>SQLException</code> is thrown.
	 *  If the given customer's ID can't be found in the DB.
	 *
	 */
	public void removeCustomer(Customer customer) throws AdminFacadeException {
		try {
			if(!customer.equals(getCustomer(customer.getId()))) {
				throw new AdminFacadeException("Customer " + customer.getId() + " details don't match");
			}
			Collection<Coupon> coupons;
			coupons = customerDAO.getCoupons(customer);
			for (Coupon coupon : coupons) {
				couponDAO.removeCouponFromCustomer(coupon, customer.getId());
			}
			customerDAO.removeCustomer(customer);
		} catch (DAOException | CustomerException | CouponException e) {
			throw new AdminFacadeException("Remove customer failed:\n" + e.getMessage(), e);
		}
	}

	/**
	 * Updates all of a customer's fields (except ID) in the DB according to the given customer bean.
	 *
	 * @param customer The customer to be updated
	 * @throws AdminFacadeException
	 *  If there is a connection problem or an <code>SQLException</code> is thrown.
	 *  If the given customer's ID can't be found in the DB (0 rows were updated).
	 */
	public void updateCustomer(Customer customer) throws AdminFacadeException {
		try {
			CustomerUtil.checkCustomer(customer);

			Customer tmpCustomer = getCustomer(customer.getId());
			tmpCustomer.setPassword(customer.getPassword());
			customerDAO.updateCustomer(tmpCustomer);
		} catch (DAOException | CustomerException e) {
			throw new AdminFacadeException("Update customer failed:\n" + e.getMessage(), e);
		}
	}

	/**
	 * Searches the DB for a customer with the given ID and
	 * returns a Customer bean with it's data from the DB.
	 *
	 * @param custID The id of the customer to find in the DB.
	 * @return {@link Customer} bean; <code>null</code> - if no customer with the given ID exists in DB
	 * @throws AdminFacadeException
	 *  If there is a connection problem or an <code>SQLException</code> is thrown.
	 *  If the given customer's ID can't be found in the DB (0 rows were returned).
	 */
	public Customer getCustomer(long custID) throws AdminFacadeException {
		try {
			return customerDAO.getCustomer(custID);
		} catch (DAOException | CustomerException e) {
			throw new AdminFacadeException("Get customer failed:\n" + e.getMessage(), e);
		}
	}

	/**
	 * Assemble and return an <code>ArrayList</code> of all the companies in the DB.
	 *
	 * @return An <code>ArrayList</code> of all the companies in DB.
	 * @throws AdminFacadeException If there is a connection problem or an <code>SQLException</code> is thrown.
	 */
	public Collection<Customer> getAllCustomers() throws AdminFacadeException{
		try {
			return customerDAO.getAllCustomers();
		} catch (DAOException e) {
			throw new AdminFacadeException("Get customers failed:\n" + e.getMessage(), e);
		}
	}

	/**
	 * checks if the customer Id already exist
	 *
	 * @param customer the customer to be checked
	 * @return true if customer id exists, false if doesn't
	 * @throws DAOException when DB error occurred
	 */
	private boolean customerIdExists(Customer customer) throws DAOException {
		// TODO Auto-generated method stub
		try {
			customerDAO.getCustomer(customer.getId());
		} catch (CustomerException e) {
			// No Customer Found
			return false;
		}
		return true;
	}

	/**
	 * checks if the customer name already exist
	 *
	 * @param customer the customer to be checked
	 * @return true if customer name exists, false if doesn't
	 * @throws DAOException when DB error occurred
	 */
	private boolean customerNameExists(Customer customer) throws DAOException {
		// TODO Auto-generated method stub
		try {
			customerDAO.getCustomerByName(customer.getCustName());
		} catch (CustomerException e) {
			// No Customer Found
			return false;
		}
		return true;
	}

	/**
	 * checks if the company Id already exist
	 *
	 * @param company the company to be checked
	 * @return true if company id exists, false if doesn't
	 * @throws DAOException when DB error occurred
	 */
	private boolean companyIdExists(Company company) throws DAOException {
		// TODO Auto-generated method stub
		try {
			companyDAO.getCompany(company.getId());
		} catch (CompanyException e) {
			// No Company Found
			return false;
		}
		return true;
	}

	/**
	 * checks if the company name already exist
	 *
	 * @param company the company to be checked
	 * @return true if company name exists, false if doesn't
	 * @throws DAOException when DB error occurred
	 */
	private boolean companyNameExists(Company company) throws DAOException {
		// TODO Auto-generated method stub
		try {
			companyDAO.getCompanyByName(company.getCompName());
		} catch (CompanyException e) {
			// No Company Found
			return false;
		}
		return true;
	}
}
