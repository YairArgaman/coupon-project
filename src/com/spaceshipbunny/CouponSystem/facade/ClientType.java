package com.spaceshipbunny.CouponSystem.facade;

/**
 * An Enum representation of Client types
 * @author Ron
 *
 */
public enum ClientType {
	ADMIN,
	COMPANY,
	CUSTOMER;
}
