package com.spaceshipbunny.CouponSystem.dbdao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.beans.CouponType;
import com.spaceshipbunny.CouponSystem.dao.CouponDAO;
import com.spaceshipbunny.CouponSystem.exceptions.CouponException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;
import com.spaceshipbunny.CouponSystem.system.ConnectionPool;


/**
 * Implements {@link CouponDAO} interface
 * @author Ron
 *
 */
public class CouponDBDAO implements CouponDAO {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static ConnectionPool pool  = ConnectionPool.getInstance();
	private static CouponDAO coupDAO = new CouponDBDAO();

	/**
	 * Private singleton constructor.
	 */
	private CouponDBDAO(){

	}

	/**
	 * Returns a static instance of CouponDBDAO
	 * @return A static instance of CouponDBDAO
	 */
	public static CouponDAO getInstance() {
		return coupDAO;
	}

	@Override
	public void createCoupon(Coupon coupon) throws DAOException, CouponException {
		// TODO Auto-generated method stub

		Connection con = pool.getConnection();

		String sql = "INSERT INTO coupon VALUES(?,?,?,?,?,?,?,?)";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, coupon.getTitle());
			stmt.setDate(2, coupon.getStartDate());
			stmt.setDate(3, coupon.getEndDate());
			stmt.setInt(4, coupon.getAmount());
			stmt.setString(5,coupon.getType().toString());
			stmt.setString(6, coupon.getMessage());
			stmt.setDouble(7, coupon.getPrice());
			stmt.setString(8, coupon.getImage());
			int dml = stmt.executeUpdate();
			//TODO add check to see how many rows were updated
			if(dml==0) {
				throw new CouponException("add coupon to coupon failed, ID  : " + coupon.getId());
			}
		} catch (SQLException e) {
			throw new DAOException("add coupon to coupon failed : ", e);
		}finally {
			pool.returnConnection(con);
		}
	}

	@Override
	public void removeCoupon(Coupon coupon) throws DAOException, CouponException {
		// TODO Auto-generated method stub
		Connection con = pool.getConnection();

		String sql = "DELETE FROM coupon WHERE ID = ?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setLong(1, coupon.getId());
			int dml = stmt.executeUpdate();
			//TODO add check to see how many rows were updated
			if(dml==0) {
				throw new CouponException("remove coupon from coupon failed, ID  : " + coupon.getId());
			}
		} catch (SQLException e) {
			throw new DAOException("remove coupon from coupon failed : ", e);
		}finally {
			pool.returnConnection(con);
		}
	}

	@Override
	public synchronized void updateCoupon(Coupon coupon) throws DAOException, CouponException {
		// TODO Auto-generated method stub
		Connection con = pool.getConnection();

//		String sql = "UPDATE coupon SET END_DATE=?, AMOUNT=?, PRICE=? WHERE ID=?";
		String sql = "UPDATE coupon SET  ID=?, TITLE=?, START_DATE=?, END_DATE=?, AMOUNT=?, TYPE=?,"
				+ "MESSAGE=?, PRICE=?, IMAGE=? WHERE ID=?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setLong(1, coupon.getId());
			stmt.setString(2, coupon.getTitle());
			stmt.setDate(3, coupon.getStartDate());
			stmt.setDate(4, coupon.getEndDate());
			stmt.setInt(5, coupon.getAmount());
			stmt.setString(6,String.valueOf(coupon.getType()));
			stmt.setString(7, coupon.getMessage());
			stmt.setDouble(8, coupon.getPrice());
			stmt.setString(9, coupon.getImage());
			stmt.setLong(10, coupon.getId());
			int dml = stmt.executeUpdate();
			//TODO add check to see how many rows were updated
			if(dml==0) {
				throw new CouponException("update coupon failed, ID  : " + coupon.getId());
			}
		} catch (SQLException e) {
//			e.printStackTrace();
			throw new DAOException("update coupon failed : ", e);
		}finally {
			pool.returnConnection(con);
		}
	}

	@Override
	public synchronized void purchaseCoupon(Coupon coupon, long custId) throws DAOException, CouponException {
		int amount = getCoupon(coupon.getId()).getAmount();

		if(amount<1)
			throw new CouponException("Coupon out of Stock");

		Connection con = pool.getConnection();

		String sql = "INSERT INTO customer_coupon VALUES(?,?)";
		String sql2 = "UPDATE coupon SET AMOUNT=?";

		try( PreparedStatement stmt = con.prepareStatement(sql);
		     PreparedStatement stmt2 = con.prepareStatement(sql2) ) {
			stmt.setLong(1, custId);
			stmt.setLong(2, coupon.getId());
			int dml = stmt.executeUpdate();
			if(dml==0) {
				throw new CouponException("add coupon to customer failed, ID  : " + coupon.getId());
			}
			stmt2.setInt(1, -1);
			int dml2 = stmt2.executeUpdate();
			if(dml2==0) {
				throw new CouponException("update coupon failed, ID  : " + coupon.getId());
			}
		} catch (SQLException e) {
			throw new DAOException("purchase coupon failed : ", e);
		}finally {
			pool.returnConnection(con);
		}
	}

	@Override
	public Coupon getCoupon(long couponID) throws DAOException, CouponException {
		Connection con = pool.getConnection();
		Coupon coupon = null;

		String sql = "SELECT * FROM coupon WHERE id =?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setLong(1, couponID);
			ResultSet rs =  stmt.executeQuery();
			if(rs.next()) {
				coupon = readCoupon(rs);
				rs.close();
				return coupon;
			}else {
				rs.close();
				throw new CouponException("get coupon failed, ID : " + couponID);
			}
		} catch (SQLException e) {
			throw new DAOException("get coupon failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	@Override
	public Coupon getCouponByTitle(String title) throws DAOException, CouponException {
		Connection con = pool.getConnection();
		Coupon coupon = null;

		String sql = "SELECT * FROM coupon WHERE TITLE =?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, title);
			ResultSet rs =  stmt.executeQuery();
			if(rs.next()) {
				coupon = readCoupon(rs);
				rs.close();
				return coupon;
			}else {
				rs.close();
				throw new CouponException("get coupon failed, Title : " + title);
			}
		} catch (SQLException e) {
			throw new DAOException("get coupon by title failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	@Override
	public Collection<Coupon> getAllCoupons() throws DAOException {
		Collection<Coupon> coupons = new ArrayList<Coupon>();
		Connection con = pool.getConnection();

		try (Statement stmt = con.createStatement();){
			ResultSet rs = stmt.executeQuery("SELECT * FROM coupon");
			while(rs.next()) {
				coupons.add(readCoupon(rs));
			}
			rs.close();
		} catch (SQLException e) {
			throw new DAOException("get all coupons failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
		return coupons;
	}

	@Override
	public Collection<Coupon> getCouponsByType(CouponType type) throws DAOException {
		Collection<Coupon> coupons = new ArrayList<Coupon>();
		Connection con = pool.getConnection();

		String sql ="SELECT * FROM coupon WHERE TYPE =?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, type.toString());
			ResultSet rs =  stmt.executeQuery();
			while(rs.next()) {
				coupons.add(readCoupon(rs));
			}
			rs.close();
		} catch (SQLException e) {
			throw new DAOException("get coupons by type failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
		return coupons;
	}

	@Override
	public void addCouponToCompany(Coupon coupon, long compId) throws DAOException, CouponException {
		Connection con = pool.getConnection();

		String sql = "INSERT INTO company_coupon VALUES(?,?)";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setLong(1, compId);
			stmt.setLong(2, coupon.getId());
			int dml = stmt.executeUpdate();
			//TODO add check to see how many rows were updated
			if(dml==0) {
				throw new CouponException("add coupon to company failed, ID  : " + coupon.getId());
			}
		} catch (SQLException e) {
			throw new DAOException("add coupon to company failed : ", e);
		}finally {
			pool.returnConnection(con);
		}
	}

	@Override
	public void removeCouponFromCompanies(Coupon coupon) throws DAOException, CouponException {
		Connection con = pool.getConnection();

		String sql = "DELETE FROM company_coupon WHERE coupon_ID = ?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setLong(1, coupon.getId());
			int dml = stmt.executeUpdate();
			//TODO add check to see how many rows were updated
			if(dml==0) {
				throw new CouponException("remove coupon from companies failed, ID  : " + coupon.getId());
			}
		} catch (SQLException e) {
			throw new DAOException("remove coupon from companies failed : ", e);
		}finally {
			pool.returnConnection(con);
		}
	}

	@Override
	public void removeCouponFromCustomer(Coupon coupon, long custId) throws DAOException, CouponException {
		Connection con = pool.getConnection();

		String sql = "DELETE FROM customer_coupon WHERE cust_id = ? AND coupon_ID = ? ";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setLong(1, custId);
			stmt.setLong(2, coupon.getId());
			int dml = stmt.executeUpdate();
			//TODO add check to see how many rows were updated
			if(dml==0) {
				throw new CouponException("remove coupon from customer failed, ID  : " + coupon.getId());
			}
		} catch (SQLException e) {
			throw new DAOException("remove coupon from customer failed : ", e);
		}finally {
			pool.returnConnection(con);
		}
	}

	@Override
	public void removeCouponFromCustomers(Coupon coupon) throws DAOException, CouponException {
		Connection con = pool.getConnection();

		String sql = "DELETE FROM customer_coupon WHERE coupon_ID = ? ";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setLong(1, coupon.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new CouponException("remove coupon from customers failed : ", e);
		}finally {
			pool.returnConnection(con);
		}
	}

	/**
	 * Reads a ResultSet of a specific coupon from the DB and into a {@link Coupon} object
	 * @param rs ResultSet of a single coupon
	 * @return A Coupon object representation of the coupon
	 * @throws SQLException If reading information from the ResultSet fails
	 */
	public static Coupon readCoupon(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Coupon coupon = new Coupon();
		coupon.setId(rs.getLong("ID"));
		coupon.setTitle(rs.getString("TITLE"));
		coupon.setStartDate(rs.getDate("START_DATE"));
		coupon.setEndDate(rs.getDate("END_DATE"));
		coupon.setAmount(rs.getInt("AMOUNT"));
		coupon.setType(CouponType.valueOf(rs.getString("TYPE")));
		coupon.setMessage(rs.getString("MESSAGE"));
		coupon.setPrice(rs.getDouble("PRICE"));
		coupon.setImage(rs.getString("IMAGE"));
		return coupon;
	}

}

