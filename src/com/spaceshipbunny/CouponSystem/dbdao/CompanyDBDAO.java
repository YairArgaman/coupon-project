package com.spaceshipbunny.CouponSystem.dbdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.spaceshipbunny.CouponSystem.beans.Company;
import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.beans.CouponType;
import com.spaceshipbunny.CouponSystem.dao.CompanyDAO;
import com.spaceshipbunny.CouponSystem.exceptions.CompanyException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;
import com.spaceshipbunny.CouponSystem.system.ConnectionPool;

/**
 * Implements {@link CompanyDAO} interface
 * @author Hagai
 *
 */
public class CompanyDBDAO implements CompanyDAO{

	private static final long serialVersionUID = 1L;
	private static ConnectionPool pool = ConnectionPool.getInstance();
	private static CompanyDBDAO instance = new CompanyDBDAO();

	/**
	 * Private constructor.
	 */
	private CompanyDBDAO() {

	}

	/**
	 * Returns a static instance of CompanyDBDAO
	 * @return A static instance of CompanyDBDAO
	 */
	public static CompanyDBDAO getInstance() {
		return instance;
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CompanyDAO#createCompany(coupon.system.beans.Company)
	 */
	public void createCompany(Company company) throws DAOException, CompanyException {
		Connection con = pool.getConnection();
		String sqlInsert = "INSERT INTO company VALUES(?,?,?,?)";
		try (PreparedStatement pstmt = con.prepareStatement(sqlInsert);) {
			pstmt.setLong(1, company.getId());
			pstmt.setString(2, company.getCompName());
			pstmt.setString(3, company.getPassword());
			pstmt.setString(4, company.getEmail());
			if (pstmt.executeUpdate() == 0) {
				throw new CompanyException("0 rows were insert");
			}
		} catch (SQLException e) {
			throw new DAOException("create company failed", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CompanyDAO#removeCompany(coupon.system.beans.Company)
	 */
	@Override
	public void removeCompany(Company company) throws DAOException, CompanyException {
		Connection con = pool.getConnection();
		try (Statement stmt = con.createStatement();) {
			String sql = "DELETE FROM company WHERE id = " + company.getId();
			if (stmt.executeUpdate(sql) == 0) {
				throw new CompanyException("0 rows were remove");
			}
		} catch (SQLException e) {
			throw new DAOException("remove company failed", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CompanyDAO#updateCompany(coupon.system.beans.Company)
	 */
	@Override
	public void updateCompany(Company company) throws DAOException, CompanyException {

		Connection con = pool.getConnection();
		try (Statement stmt = con.createStatement();) {
			String sql = "UPDATE company " + "SET password ='" + company.getPassword() + "', email='"
					+ company.getEmail() + "' comp_name ='" + company.getCompName() + "' WHERE id=" + company.getId();
			if (stmt.executeUpdate(sql) == 0) {
				throw new CompanyException("0 rows were update");
			}

		} catch (SQLException e) {
			throw new DAOException("update company failed", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CompanyDAO#getCompany(long)
	 */
	@Override
	public Company getCompany(long id) throws DAOException, CompanyException {
		Connection con = pool.getConnection();

		try (Statement stmt = con.createStatement();) {
			String sql = "SELECT * " + "FROM company " + "WHERE id = " + id;
			ResultSet set = stmt.executeQuery(sql);
			if (set.next()) {
				Company company = readCompany(set);
				return company;
			} else {
				throw new CompanyException("can't find company with id num : " + id);
			}
		} catch (SQLException e) {
			throw new DAOException("get company by id failed", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	private Company readCompany(ResultSet set) throws SQLException {
		Company com = new Company();
		com.setId(set.getLong(1));
		com.setCompName(set.getString(2));
		com.setPassword(set.getString(3));
		com.setEmail(set.getString(4));
		return com;
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CompanyDAO#getCompanyByName(String)
	 */
	@Override
	public Company getCompanyByName(String name) throws DAOException, CompanyException {
		Connection con = pool.getConnection();
		try (Statement stmt = con.createStatement();) {
			String sql = "SELECT * " + "FROM company " + "WHERE comp_name = '" + name + "'";
			ResultSet set = stmt.executeQuery(sql);
			if (set.next()) {
				Company company = readCompany(set);
				return company;
			} else {
				throw new CompanyException("cant found company with name " + name);
			}
		} catch (SQLException e) {
			throw new DAOException("get company by name failed", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CompanyDAO#getAllCompanies()
	 */
	@Override
	public Collection<Company> getAllCompanies() throws DAOException {
		Connection con = pool.getConnection();
		List<Company> allCompanies = new ArrayList<>();
		try (Statement stmt = con.createStatement();) {
			String sql = "SELECT * FROM company";
			ResultSet set = stmt.executeQuery(sql);
			while (set.next()) {
				Company company = readCompany(set);
				allCompanies.add(company);
			}
		} catch (SQLException e) {
			throw new DAOException("get all companies failed", e);
		} finally {
			pool.returnConnection(con);
		}
		return allCompanies;
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CompanyDAO#getCoupons()
	 */
	@Override
	public Collection<Coupon> getCoupons(long companyId) throws DAOException {
		Connection con = pool.getConnection();
		List<Coupon> couponList = new ArrayList<>();
		try (Statement stmt = con.createStatement();) {
			String sql = "SELECT * " + "FROM company_coupon RIGHT JOIN coupon "
					+ "ON coupon.id = company_coupon.coupon_id " + "WHERE comp_id = " + companyId;
			ResultSet set = stmt.executeQuery(sql);

			while (set.next()) {
				Coupon coupon = new Coupon();
				coupon.setId(set.getLong("ID"));
				coupon.setTitle(set.getString("TITLE"));
				coupon.setStartDate(set.getDate(5));
				coupon.setEndDate(set.getDate(6));
				coupon.setAmount(set.getInt("AMOUNT"));
				CouponType couponType = CouponType.valueOf(set.getString("TYPE"));
				coupon.setType(couponType);
				coupon.setMessage(set.getString(9));
				coupon.setPrice(set.getLong(10));
				coupon.setImage(set.getString(11));
				couponList.add(coupon);
			}
		} catch (SQLException e) {
			throw new DAOException("get all companies failed", e);
		} finally {
			pool.returnConnection(con);
		}
		return couponList;
	}

	/**
	 * Returns true if the given company user name is in the DB and if the given
	 * password is equal to the password in the DB (same row as the company name)
	 *
	 * @param compName The company's user name
	 * @param password The company's password
	 * @return <code>true</code> if user name and password match; otherwise <code>false</code>
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 *
	 */
	public static boolean login(String compName, String password) throws DAOException {
		Connection con = pool.getConnection();
		try (Statement stmt = con.createStatement();) {
			String sql = "SELECT password " + "FROM company " + "where comp_name = '" + compName + "'";
			ResultSet set = stmt.executeQuery(sql);
			if (set.next()) {
				if (set.getString(1).equals(password)) {
					return true;
				}
			}
			return false;
		} catch (SQLException e) {
			throw new DAOException("login failed", e);
		} finally {
			pool.returnConnection(con);
		}
	}

}
