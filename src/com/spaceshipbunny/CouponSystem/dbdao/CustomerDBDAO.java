package com.spaceshipbunny.CouponSystem.dbdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.beans.Customer;
import com.spaceshipbunny.CouponSystem.dao.CustomerDAO;
import com.spaceshipbunny.CouponSystem.exceptions.CustomerException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;
import com.spaceshipbunny.CouponSystem.system.ConnectionPool;

/**
 * Implements {@link CustomerDAO} interface
 * @author Yair
 *
 */
public class CustomerDBDAO implements CustomerDAO{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static ConnectionPool pool = ConnectionPool.getInstance();
	private static CustomerDAO custDAO = new CustomerDBDAO();

	/**
	 * Private singleton constructor.
	 */
	private CustomerDBDAO() {
	}

	/**
	 * Returns a static instance of CustomerDBDAO
	 * @return A static instance of CustomerDBDAO
	 */
	public static CustomerDAO getInstance() {
		return custDAO;
	}

	/**
	 * Returns true if the given customer user name is in the DB and if the given
	 * password is equal to the password in the DB (same row as the customer name)
	 *
	 * @param custName The customer's user name
	 * @param password The customer's password
	 * @return <code>true</code> if user name and password match; otherwise <code>false</code>
	 * @throws DAOException If there is a connection problem or an <code>SQLException</code> is thrown.
	 *
	 */
	public static boolean login(String custName, String password) throws DAOException {

		Connection con = pool.getConnection();

		String sql = "SELECT id FROM customer WHERE CUST_NAME=? AND PASSWORD=?";
		try (PreparedStatement stmt = con.prepareStatement(sql)){
			stmt.setString(1, custName);
			stmt.setString(2, password);
			ResultSet rs = stmt.executeQuery();

			if(rs.next()) {
				rs.close();
				return true;
			}
		} catch (SQLException e) {
			throw new DAOException("customer login failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CustomerDAO#createCustomer(coupon.system.beans.Customer)
	 */
	@Override
	public void createCustomer(Customer customer) throws DAOException, CustomerException {
		Connection con = pool.getConnection();

		String sql = "INSERT INTO customer VALUES(?,?,?)";
		try(PreparedStatement stmt = con.prepareStatement(sql)){
			stmt.setLong(1, customer.getId());
			stmt.setString(2, customer.getCustName());
			stmt.setString(3, customer.getPassword());
			int dml = stmt.executeUpdate();
			//TODO add check to see how many rows were updated
			if(dml==0) {
				throw new CustomerException("create customer failed, ID : " + customer.getId());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new DAOException("create customer failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CustomerDAO#removeCustomer(coupon.system.beans.Customer)
	 */
	@Override
	public void removeCustomer(Customer customer) throws DAOException, CustomerException {
		Connection con = pool.getConnection();

		String sql = "DELETE FROM customer WHERE id = ?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setLong(1, customer.getId());
			int dml = stmt.executeUpdate();
			//TODO add check to see how many rows were updated
			if(dml==0) {
				throw new CustomerException("remove customer failed, ID : " + customer.getId());
			}
		} catch (SQLException e) {
			throw new DAOException("remove customer failed : ", e);
		} finally {
			pool.returnConnection(con);
		}

	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CustomerDAO#updateCustomer(coupon.system.beans.Customer)
	 */
	@Override
	public void updateCustomer(Customer customer) throws DAOException, CustomerException {
		Connection con = pool.getConnection();

//		String sql = "UPDATE customer SET PASSWORD=? WHERE ID=?";
		String sql = "UPDATE customer SET ID=?, CUST_NAME=?, PASSWORD=? WHERE ID=?";
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
/*			stmt.setString(1, customer.getPassword());
			stmt.setLong(2, customer.getId());*/
			stmt.setLong(1, customer.getId());
			stmt.setString(2, customer.getCustName());
			stmt.setString(3, customer.getPassword());
			stmt.setLong(4, customer.getId());
			int dml = stmt.executeUpdate();
			//TODO add check to see how many rows were updated
			if(dml==0) {
				throw new CustomerException("update customer failed, ID : " + customer.getId());
			}
		} catch (SQLException e) {
			throw new DAOException("update customer failed : ", e);
		} finally {
			pool.returnConnection(con);
		}

	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CustomerDAO#getCustomer(long)
	 */
	@Override
	public Customer getCustomer(long custId) throws DAOException, CustomerException {
		Connection con = pool.getConnection();
		Customer customer = null;

		String sql = "SELECT * FROM customer WHERE ID=?";
		try (PreparedStatement stmt = con.prepareStatement(sql)){
			stmt.setLong(1, custId);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				customer = readCustomer(rs);
				rs.close();
				return customer;
			}else {
				rs.close();
				throw new CustomerException("get customer failed,  ID : " + custId);
			}
		} catch (SQLException e) {
			throw new DAOException("get customer failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CustomerDAO#getCustomerByName(String)
	 */
	public Customer getCustomerByName(String custName) throws DAOException, CustomerException {
		Connection con = pool.getConnection();
		Customer customer=null;

		String sql = "SELECT * FROM customer WHERE CUST_NAME=?";
		try (PreparedStatement stmt = con.prepareStatement(sql)){
			stmt.setString(1, custName);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				customer = readCustomer(rs);
				rs.close();
				return customer;
			}else {
				rs.close();
				throw new CustomerException("get customer failed, name : " + custName);
			}
		} catch (SQLException e) {
			throw new DAOException("get customer by name failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CustomerDAO#getCoupons()
	 */
	@Override
	public Collection<Coupon> getCoupons(Customer customer) throws DAOException {
		Collection<Coupon> coupons = new ArrayList<Coupon>();
		Connection con = pool.getConnection();

		String sql="SELECT coupon.* FROM coupon "
				+"INNER JOIN customer_coupon ON coupon.id = customer_coupon.coupon_id "
				+ "WHERE customer_coupon.cust_id =?";
		try (PreparedStatement stmt = con.prepareStatement(sql)){
			stmt.setLong(1, customer.getId());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				coupons.add(CouponDBDAO.readCoupon(rs));
			}
			rs.close();
		} catch (SQLException e) {
			throw new DAOException("get customer coupons failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
		return coupons;
	}

	/* (non-Javadoc)
	 * @see coupon.system.dao.CustomerDAO#getAllCustomers()
	 */
	@Override
	public Collection<Customer> getAllCustomers() throws DAOException {
		Collection<Customer> customers = new ArrayList<Customer>();
		Connection con = pool.getConnection();
		Customer customer = null;

		String sql = "SELECT * FROM customer";
		try (Statement stmt = con.createStatement();){
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				customer = readCustomer(rs);
				customers.add(customer);
			}
			rs.close();
		} catch (SQLException e) {
			throw new DAOException("get all customers failed : ", e);
		} finally {
			pool.returnConnection(con);
		}
		return customers;

	}

	/**
	 * Reads a ResultSet of a specific customer from the DB and into a {@link Customer} object
	 * @param rs ResultSet of a single customer
	 * @return A {@link Customer} object representation of the customer
	 * @throws SQLException If reading information from the ResultSet fails
	 */
	private Customer readCustomer(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Customer customer = new Customer();
		customer.setId(rs.getLong("ID"));
		customer.setCustName(rs.getString("CUST_NAME"));
		customer.setPassword(rs.getString("PASSWORD"));
		return customer;
	}
}
