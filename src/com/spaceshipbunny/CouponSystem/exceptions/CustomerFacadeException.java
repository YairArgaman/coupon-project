package com.spaceshipbunny.CouponSystem.exceptions;

public class CustomerFacadeException extends FacadeException {

	public CustomerFacadeException() {
		// TODO Auto-generated constructor stub
	}

	public CustomerFacadeException(String message, Throwable cause, boolean enableSuppression,
	                               boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CustomerFacadeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CustomerFacadeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CustomerFacadeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
