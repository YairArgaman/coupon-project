package com.spaceshipbunny.CouponSystem.exceptions;

/**
 * The CouponException is an exception used when an exception occurs in the CouponFacade.
 */
public class CouponException extends BeanException {
	private static final long serialVersionUID = 1L;

	public CouponException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CouponException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CouponException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CouponException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CouponException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
