package com.spaceshipbunny.CouponSystem.exceptions;

public class BeanException extends CouponSystemException {

	/**
	 * The BeanException is an exception used when an exception occurs in one of the beans.
	 */
	private static final long serialVersionUID = 1L;

	public BeanException() {
		// TODO Auto-generated constructor stub
	}

	public BeanException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public BeanException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BeanException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BeanException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
