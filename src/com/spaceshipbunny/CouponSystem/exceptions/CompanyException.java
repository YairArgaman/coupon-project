package com.spaceshipbunny.CouponSystem.exceptions;

/**
 * The CompanyException is an exception used when an exception occurs in the CompanyFacade.
 */
public class CompanyException extends BeanException {
	private static final long serialVersionUID = 1L;

	/**
	 * Public exception constructor
	 */
	public CompanyException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CompanyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CompanyException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CompanyException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CompanyException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
