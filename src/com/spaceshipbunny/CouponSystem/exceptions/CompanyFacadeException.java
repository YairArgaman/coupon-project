package com.spaceshipbunny.CouponSystem.exceptions;

public class CompanyFacadeException extends FacadeException {

	public CompanyFacadeException() {
		// TODO Auto-generated constructor stub
	}

	public CompanyFacadeException(String message, Throwable cause, boolean enableSuppression,
	                              boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CompanyFacadeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CompanyFacadeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CompanyFacadeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
