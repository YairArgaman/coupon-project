package com.spaceshipbunny.CouponSystem.exceptions;

/**
 * The AdminException is an exception used when an exception occurs in the AdminFacade.
 */
public class AdminFacadeException extends FacadeException {

	private static final long serialVersionUID = 3331008703450665483L;

	/**
	 * Public empty constructor
	 */
	public AdminFacadeException() {
		// TODO Auto-generated constructor stub
	}

	public AdminFacadeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AdminFacadeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AdminFacadeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AdminFacadeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}