package com.spaceshipbunny.CouponSystem.exceptions;

public class FacadeException extends CouponSystemException {

	private static final long serialVersionUID = 4430643016882754501L;

	public FacadeException() {
		// TODO Auto-generated constructor stub
	}

	public FacadeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public FacadeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FacadeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FacadeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
