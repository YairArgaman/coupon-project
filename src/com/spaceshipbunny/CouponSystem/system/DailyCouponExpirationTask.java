package com.spaceshipbunny.CouponSystem.system;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;

import com.spaceshipbunny.CouponSystem.beans.Coupon;
import com.spaceshipbunny.CouponSystem.dao.CouponDAO;
import com.spaceshipbunny.CouponSystem.dbdao.CouponDBDAO;
import com.spaceshipbunny.CouponSystem.exceptions.CouponException;
import com.spaceshipbunny.CouponSystem.exceptions.DAOException;

/**
 * A daily task that checks and deletes all the coupons in accordance to their expiration date.
 *
 * @author Ron
 *
 */
public class DailyCouponExpirationTask implements Runnable , Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final static DailyCouponExpirationTask task = new DailyCouponExpirationTask();
	private final Thread t;



	public Thread getT() {
		return t;
	}

	private CouponDAO couponDAO  = CouponDBDAO.getInstance();
	private boolean quit = false;

	/**
	 * Private constructor
	 * @throws DAOException
	 *
	 */
	private DailyCouponExpirationTask(){
		this.t = new Thread(this);
		this.t.start();
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		System.out.println("LOG : Daily Task started");
		while(!quit) {
			try {
				//Sets the next iteration for 00:00 hour
				Thread.sleep((long)(System.currentTimeMillis()%(24*60*60*1000) + (2*60*60*1000)));
			} catch (InterruptedException e) {
				// TODO Manager handling
				// e.printStackTrace();
				System.err.println("Daily task sleep interrupted : " + e);
			}

			long time = System.currentTimeMillis();

			try {
				Collection<Coupon> coupons = couponDAO.getAllCoupons();
				for (Coupon coupon : coupons) {
					if (coupon.getEndDate().before(new Date(time))) {
						couponDAO.removeCouponFromCustomers(coupon);
						couponDAO.removeCouponFromCompanies(coupon);
						couponDAO.removeCoupon(coupon);
					}
				}
			} catch (DAOException | CouponException e) {
				// TODO Manager handling
				// e.printStackTrace();
				System.err.println("Daily task incomplited : " + e);
			}
		}
		System.out.println("LOG : Daily Task ended");
	}

	/**
	 * Stops the task.
	 */
	public void stopTask() {
		System.out.println("LOG : Daily Task stopped");
		quit = true;
		t.interrupt();
	}

	/**
	 * Returns an instance of the task
	 * @return An instance of the task
	 */
	public static DailyCouponExpirationTask getInstance() {
		return task;
	}
}
