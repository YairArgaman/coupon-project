package com.spaceshipbunny.CouponSystem.system;

import com.spaceshipbunny.CouponSystem.exceptions.DAOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * ConnectionPool class to allow for easier/faster connection to database.
 * ConnectionPool is a static singleton class that loads up whenever the program using it starts up.
 * @author Yair Argaman
 * @version 0.1
 */
public final class ConnectionPool {
	private static ConnectionPool INSTANCE;
	private String url;
	private String user;
	private String password;
	private List<Connection> connectionPool;
	private List<Connection> usedConnections = new ArrayList<>( );
	private static int INITIAL_POOL_SIZE = 4;
	private static final int MAX_POOL_SIZE = 10;

	/* On startup of program */ // TODO Change location of startup?
	static {
		try {
			INSTANCE = create( "jdbc:mysql://localhost:3306/Coupon_System",
							  "admin",
						  "1234" );  // Oh, the horror.
		} catch (SQLException e) {
			System.err.println( e.getMessage()+ ": " + e.getCause( ) );
		}
	}

	/* Constructor */
	/**
	 * ConnectionPool constructor
	 * @param url Connection URL
	 * @param user Login username
	 * @param password Login password
	 * @param connectionPool ConnectionPool - An ArrayList of connections
	 */
	private ConnectionPool( String url, String user, String password, List<Connection> connectionPool ) {
		this.url = url;
		this.user = user;
		this.password = password;
		this.connectionPool = connectionPool;
	}

	/**
	 * Creates and initializes a ConnectionPool with connections
	 * @param url Connection URL
	 * @param user Login username
	 * @param password Login password
	 * @return An initialized ConnectionPool instance
	 * @throws SQLException If the creation of any connection fails
	 */
	private static ConnectionPool create( String url, String user, String password ) throws SQLException {
		try {
			List<Connection> pool = new ArrayList<>( INITIAL_POOL_SIZE );
			for ( int i = 0; i < INITIAL_POOL_SIZE; i++ ) {
				pool.add( createConnection( url, user, password ) );
			}
			return new ConnectionPool( url, user, password, pool );
		} catch (SQLException e) {
			throw new SQLException( "Failed to create a connection pool! Please check if your server is set up correctly.\n\t", e );
		}

	}

	/**
	 * Gets a connection from the ConnectionPool. If none are available, method will wait for a connection to be available.
	 * @return A connection.
	 * @throws InterruptedException If method's thread is interrupted.
	 */
	synchronized public Connection getConnection( ) throws DAOException {
		try {
			while ( connectionPool.size() <= 0 ) {
				this.wait();
			}
			if ( connectionPool.isEmpty() ) {
				if ( usedConnections.size() < MAX_POOL_SIZE ) {
					connectionPool.add( createConnection( url, user, password ) );
				} else {
					throw new DAOException( "Maximum pool size reached, no available connections!" );
				}
			}
			Connection connection = connectionPool.remove( connectionPool.size() - 1 );
			usedConnections.add( connection );
			return connection;
		} catch ( InterruptedException e ) {
			throw new DAOException( "Get connection failed! Method thread interrupted! ", e );
		} catch (SQLException e) {
			throw new DAOException( "Get connection failed! ", e );
		}
	}

	/**
	 * Returns control of the given connection back to the ConnectionPool.
	 * @param connection The connection to give up.
	 * @return <tt>true</tt> - If control of the connection has been successfully returned to the ConnectionPool.
	 */
	public synchronized boolean returnConnection( Connection connection ) {
		connectionPool.add( connection );
		this.notify( );
		return usedConnections.remove( connection );
	}

	/**
	 * Close all connections that are either available or in use.
	 * @throws SQLException If a connection fails to close or remove.
	 */
	public synchronized void closeAllConnections( ) throws SQLException {
		Connection connection;
		closeAllConnections(connectionPool);
		closeAllConnections(usedConnections);
	}

	/**
	 * Close all connections in a given List of Connections.
	 * @param connectionPool The List of Connections to close.
	 * @throws SQLException If a connection fails to close or remove from the given Connection List.
	 */
	private void closeAllConnections( List<Connection> connectionPool ) throws SQLException {
		Connection connection;
		for (int i = connectionPool.size() - 1; i >= 0 ; i--) {
			System.out.printf("Closing an available connection out of %d remaining. ", i );
			connection = connectionPool.get(i);
			connection.close();
			connectionPool.remove(connection);
			System.out.println("Success!");
		}
	}

	/**
	 * Creates and returns a connection.
	 * @param url Connection URL.
	 * @param user Login username.
	 * @param password Login password.
	 * @return The created connection.
	 * @throws SQLException If the connection to the database fails.
	 */
	private static Connection createConnection( String url, String user, String password ) throws SQLException {
		return DriverManager.getConnection( url, user, password );
	}

	/**
	 * Returns the total number of connections available and in use.
	 * @return The total number of connections available and in use.
	 */
	public int getSize( ) {
		return connectionPool.size( ) + usedConnections.size( );
	}

	/**
	 * Returns the singleton instance of ConnectionPool.
	 * @return The singleton instance of ConnectionPool.
	 */
	public static ConnectionPool getInstance( ) {
		return INSTANCE;
	}

	/**
	 * Closes all available and used connections, discards the previous instance of {@link ConnectionPool},
	 * and initializes a new {@link ConnectionPool} with new connections
	 * @param url Connection URL
	 * @param userName Login username
	 * @param password Login password
	 * @return A new initialized ConnectionPool instance
	 * @throws DAOException If the creation of a new ConnectionPool fails
	 */
	public ConnectionPool changeConnectionPoolConnections( String url, String userName, String password ) throws DAOException {
		try {
			closeAllConnections();
			INSTANCE = create( url, userName, password );
			return INSTANCE;
		} catch (SQLException e) {
			throw new DAOException("Connection pool update failed! ", e);
		}
	}
}